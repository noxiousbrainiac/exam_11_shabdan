import {
    FETCH_PRODUCT_FAILURE,
    FETCH_PRODUCT_REQUEST,
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS,
    POST_PRODUCT_FAILURE,
    POST_PRODUCT_REQUEST,
    POST_PRODUCT_SUCCESS, REMOVE_PRODUCT_FAILURE,
    REMOVE_PRODUCT_REQUEST,
    REMOVE_PRODUCT_SUCCESS
} from "../actions/productsActions";

const initialState = {
    productsError: null,
    productError: null,
    postError: null,
    loading: false,
    products: [],
    product: null,
    removeError: null
};

const productsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PRODUCTS_REQUEST:
            return {...state, loading: true};
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, loading: false, products: action.payload};
        case FETCH_PRODUCTS_FAILURE:
            return {...state, loading: false, productsError: action.payload};
        case FETCH_PRODUCT_REQUEST:
            return {...state, loading: true};
        case FETCH_PRODUCT_FAILURE:
            return {...state, loading: false, productError: action.payload};
        case FETCH_PRODUCT_SUCCESS:
            return {...state, loading: false, product: action.payload};
        case POST_PRODUCT_REQUEST:
            return {...state, loading: true};
        case POST_PRODUCT_SUCCESS:
            return {...state, loading: false};
        case POST_PRODUCT_FAILURE:
            return {...state, loading: false, postError: action.payload};
        case REMOVE_PRODUCT_REQUEST:
            return {...state, loading: true};
        case REMOVE_PRODUCT_SUCCESS:
            return {...state, loading: false};
        case REMOVE_PRODUCT_FAILURE:
            return {...state, loading: false, removeError: action.payload};
        default: return state;
    }
};

export default productsReducer;