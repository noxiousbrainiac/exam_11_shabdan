import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import {historyPush} from "./historyActions";

export const FETCH_PRODUCTS_REQUEST = "FETCH_PRODUCTS_REQUEST";
export const FETCH_PRODUCTS_SUCCESS = "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_FAILURE = "FETCH_PRODUCTS_FAILURE";

export const FETCH_PRODUCT_REQUEST = "FETCH_PRODUCT_REQUEST";
export const FETCH_PRODUCT_SUCCESS = "FETCH_PRODUCT_SUCCESS";
export const FETCH_PRODUCT_FAILURE = "FETCH_PRODUCT_FAILURE";

export const POST_PRODUCT_REQUEST = "POST_PRODUCT_REQUEST";
export const POST_PRODUCT_SUCCESS = "POST_PRODUCT_SUCCESS";
export const POST_PRODUCT_FAILURE = "POST_PRODUCT_FAILURE";

export const REMOVE_PRODUCT_REQUEST = "REMOVE_PRODUCT_REQUEST";
export const REMOVE_PRODUCT_SUCCESS = "REMOVE_PRODUCT_SUCCESS";
export const REMOVE_PRODUCT_FAILURE = "REMOVE_PRODUCT_FAILURE";

export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = (products) => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = (error) => ({type: FETCH_PRODUCTS_FAILURE, payload: error});

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = (product) => ({type: FETCH_PRODUCT_SUCCESS, payload: product});
export const fetchProductFailure = (error) => ({type: FETCH_PRODUCT_FAILURE, payload: error});

export const postProductRequest = () => ({type: POST_PRODUCT_REQUEST});
export const postProductSuccess = () => ({type: POST_PRODUCT_SUCCESS});
export const postProductFailure = (error) => ({type: POST_PRODUCT_FAILURE, payload: error});

export const removeProductRequest = () => ({type: REMOVE_PRODUCT_REQUEST});
export const removeProductSuccess = () => ({type: REMOVE_PRODUCT_SUCCESS});
export const removeProductFailure = (error) => ({type: REMOVE_PRODUCT_FAILURE, payload: error});

export const fetchProducts = (category) => async (dispatch) => {
    try {
        dispatch(fetchProductsRequest());
        let response;
        if (category) {
            response = await axiosApi.get(`/products?category=${category}`);
        } else {
            response = await axiosApi.get(`/products`);
        }
        dispatch(fetchProductsSuccess(response.data));
    } catch (e) {
        dispatch(fetchProductsFailure(e));
        if (e.response.status === 404) {
            toast.warning('No products here!');
        } else {
            toast.error('Could not fetch data!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const fetchProduct = (id) => async (dispatch) => {
    try {
        dispatch(fetchProductRequest());
        const {data} = await axiosApi.get(`/products/${id}`);
        dispatch(fetchProductSuccess(data));
    } catch (e) {
        dispatch(fetchProductFailure(e));
        if (e.response.status === 404) {
            toast.warning(`No product's data here!`);
        } else {
            toast.error('Could not fetch data!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const postProduct = (productData) => async (dispatch) => {
    try {
        dispatch(postProductSuccess());
        await axiosApi.post(`/products`, productData);
        dispatch(postProductSuccess());
        toast.success('Product created successful');
    } catch (e) {
        dispatch(postProductFailure(e));
        if (e.response.status === 401) {
            toast.warning('You have to login!');
        } else if (e.response.status === 400){
            toast.warning('Enter empty inputs!');
        } else {
            toast.error(`Something wrong! ${e.response.data}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

export const removeProduct = (id) => async (dispatch) => {
    try {
        dispatch(removeProductRequest());
        await axiosApi.delete(`/products/${id}`);
        dispatch(removeProductSuccess());
        dispatch(historyPush('/'));
        toast.success('Product removed successful');
    } catch (e) {
        dispatch(removeProductFailure(e));
        if (e.response.status === 403) {
            toast.warning(e.response.data.error);
        } else if (e.response.status === 401) {
            toast.warning("You have to log in!");
        } else {
            toast.error(`Something wrong! ${e.response.data}` , {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
};

