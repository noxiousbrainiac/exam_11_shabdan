import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";

export const FETCH_CATEGORIES_REQUEST = "FETCH_CATEGORIES_REQUEST";
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_FAILURE = "FETCH_CATEGORIES_FAILURE";

export const fetchCategoriesRequest = () => ({type: FETCH_CATEGORIES_REQUEST});
export const fetchCategoriesSuccess = (categories) => ({type: FETCH_CATEGORIES_SUCCESS, payload: categories});
export const fetchCategoriesFailure = (error) => ({type: FETCH_CATEGORIES_FAILURE, payload: error});

export const fetchCategories = () => async (dispatch) => {
    try {
        dispatch(fetchCategoriesRequest());
        const {data} = await axiosApi.get('/categories');
        dispatch(fetchCategoriesSuccess(data));
    } catch (e) {
        dispatch(fetchCategoriesFailure(e));
        if (e.response.status === 404) {
            toast.warning('No categories!');
        } else {
            toast.error('Could not fetch data!', {
                theme: 'colored',
                icon: <WarningIcon/>
            });
        }
    }
}