import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import ProductForm from "../../components/ProductForm/ProductForm";
import {useDispatch, useSelector} from "react-redux";
import {fetchCategories} from "../../store/actions/categoriesActions";
import {postProduct} from "../../store/actions/productsActions";

const AddPage = ({history}) => {
    const dispatch = useDispatch();
    const categories = useSelector(state => state.categories.categories);

    useEffect(() => {
        dispatch(fetchCategories());
    }, [dispatch]);

    const onSubmit = async productData => {
        await dispatch(postProduct(productData));
        history.replace('/');
    };

    return (
        <>
            <Typography variant="h4">New product</Typography>
            <ProductForm
                onSubmit={onSubmit}
                categories={categories}
            />
        </>
    );
};

export default AddPage;