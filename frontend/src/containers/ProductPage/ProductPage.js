import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProduct, removeProduct} from "../../store/actions/productsActions";
import {Button, Grid, Typography} from "@material-ui/core";
import {apiUrl} from "../../config";

const ProductPage = ({match}) => {
    const dispatch = useDispatch();
    const product = useSelector(state => state.products.product);
    const user = useSelector(state => state.users.user);

    let cardImage = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (product?.image) {
        cardImage = apiUrl + '/' + product?.image;
    }

    const remove = () => {
        dispatch(removeProduct(match.params.id));
    }

    useEffect(() => {
        dispatch(fetchProduct(match.params.id));
    }, [dispatch, match.params.id]);


    return (
        <Grid container direction={"row"}>
            <Grid item xs={12} md={6} lg={6}>
                <img src={cardImage} alt="Product" style={{width: "100%"}}/>
            </Grid>
            <Grid item xs={12} md={6} lg={6}>
                <Typography variant='h3'>
                    {product?.title}
                </Typography>
                <Typography variant='subtitle2'>
                    {product?.description}
                </Typography>
                <Typography variant='h6'>
                    Price: {product?.price}
                </Typography>
                <Typography variant='h4'>
                    Account: {product?.user.displayName}
                </Typography>
                <Typography variant='h4'>
                    Seller's name: {product?.user.username}
                </Typography>
                <Typography variant='h4'>
                    Contact: {product?.user.phoneNumber}
                </Typography>
                {user ?
                    <Button onClick={remove} variant="contained" color="secondary">
                        Delete
                    </Button> : null
                }
            </Grid>
        </Grid>
    );
};

export default ProductPage;