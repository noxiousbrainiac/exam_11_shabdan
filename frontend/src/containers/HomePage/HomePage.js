import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, CircularProgress, Grid} from "@material-ui/core";
import ProductList from "../../components/ProductList/ProductList";
import {fetchProducts} from "../../store/actions/productsActions";
import {fetchCategories} from "../../store/actions/categoriesActions";

const HomePage = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const categories = useSelector(state => state.categories.categories);
    const loading = useSelector(state => state.products.loading);

    const clickCategory = (id) => {
        dispatch(fetchProducts(id));
    }

    useEffect(() => {
        dispatch(fetchCategories());
        dispatch(fetchProducts());
    }, [dispatch]);

    let productsContainer = (
        <ProductList products={products}/>
    )

    if (loading === true) {
        productsContainer = (
            <div
                style={{
                    position: "relative",
                    paddingLeft: "50%",
                    paddingTop: "30%"
                }}
            >
                <CircularProgress
                    color="secondary"
                    size={100}
                />
            </div>
        )
    }

    return (
        <Grid container direction="row" style={{paddingTop: "20px"}}>
            <Grid item md={2} xs={12} lg={2}>
                <ul>
                    <li>
                        <Button
                            type="button"
                            onClick={() => clickCategory()}
                        >
                            All
                        </Button>
                    </li>
                    {categories ? categories.map(item => (
                        <li key={item._id}>
                            <Button
                                type="button"
                                onClick={() => clickCategory(item._id)}
                            >
                                {item.title}
                            </Button>
                        </li>
                    )) : <h2>No categories here</h2>}
                </ul>
            </Grid>
            <Grid item md={10} xs={12} lg={10}>
                {productsContainer}
            </Grid>
        </Grid>
    );
};

export default HomePage;