import React from 'react';
import {Grid} from "@material-ui/core";
import ProductCard from "./ProductCard/ProductCard";

const ProductList = ({products}) => {
    return (
        <Grid container spacing={2}>
            {products ?
                products.map(item => (
                    <ProductCard
                        key={item._id}
                        title={item.title}
                        id={item._id}
                        description={item.description}
                        price={item.price}
                        image={item.image}
                        user={item.user.username}
                    />
                )) : <h2>No products here!</h2>
            }
        </Grid>
    );
};

export default ProductList;