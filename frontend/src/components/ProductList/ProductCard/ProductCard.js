import React from 'react';
import {
    Card,
    CardActions,
    CardContent,
    CardHeader,
    CardMedia,
    Grid,
    IconButton,
    makeStyles,
    Typography
} from "@material-ui/core";
import {apiUrl} from "../../../config";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        width: "100%"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
})

const ProductCard = ({title, description, price, user, image, id}) => {
    const classes = useStyles();

    let cardImage = "https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/9/2/8/5/235829-6-eng-GB/Feed-Test-SIC-Feed-20142_news_large.jpg";

    if (image) {
        cardImage = apiUrl + '/' + image;
    }

    return (
        <Grid item lg={6} md={6} xs={12}>
            <Card className={classes.card}>
                <CardHeader title={`${title}`}/>
                <CardMedia
                    image={cardImage}
                    className={classes.media}
                />
                <CardContent>
                    <Typography variant="h4">
                        Description: {description}
                        Price: {price}
                        Seller: {user}
                    </Typography>
                    <Typography variant="h5">
                        Seller: {user}
                    </Typography>
                    <Typography variant="h5">
                        Price: {price}
                    </Typography>
                </CardContent>
                <CardActions>
                    <IconButton component={Link} to={'/products/' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
            </Card>

        </Grid>
    );
};

export default ProductCard;