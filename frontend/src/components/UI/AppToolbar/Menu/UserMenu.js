import React, {useState} from 'react';
import {Button, makeStyles, Menu, MenuItem} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {historyPush} from "../../../../store/actions/historyActions";
import {logoutUser} from "../../../../store/actions/usersActions";

const useStyles = makeStyles({
    menuColor: {
        color: "beige"
    }
})

const UserMenu = ({user}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const toOut = () => {
        dispatch(logoutUser());
    }

    const toAddPage = () => {
        dispatch(historyPush('/addproduct'));
    }

    return (
        <>
            <Button className={classes.menuColor} aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                Hello, {user.username}!
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={toAddPage}>Add product</MenuItem>
                <MenuItem onClick={toOut}>Logout</MenuItem>
            </Menu>
        </>
    );
};

export default UserMenu;