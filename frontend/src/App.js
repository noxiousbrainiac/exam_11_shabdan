import React from 'react';
import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import Login from "./containers/Login/Login";
import ProductPage from "./containers/ProductPage/ProductPage";
import Register from "./containers/Register/Register";
import AddPage from "./containers/AddPage/AddPage";

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={HomePage}/>
                <Route path='/products/:id' component={ProductPage}/>
                <Route path='/register' component={Register}/>
                <Route path='/login' component={Login}/>
                <Route path='/addproduct' component={AddPage}/>
            </Switch>
        </Layout>
    );
};

export default App;