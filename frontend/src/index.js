import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Router} from "react-router-dom";
import {Provider} from "react-redux";
import {MuiThemeProvider} from "@material-ui/core";
import {ToastContainer} from "react-toastify";
import theme from "./theme";
import store from "./store/configureStore";
import 'react-toastify/dist/ReactToastify.css';
import history from "./history";

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));
