const post = (req, res, next) => {
    if (!req.body.title || !req.body.price || !req.body.category || !req.body.description) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const productData = {
        title: req.body.title,
        price: req.body.price,
        category: req.body.category,
        description: req.body.description,
        user: req.user._id
    };

    if (req.file) {
        productData.image = 'uploads/' + req.file.filename;
    }

    req.data = productData;

    next();
}

module.exports = post;