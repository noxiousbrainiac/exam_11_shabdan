const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routers/users');
const products = require('./routers/products');
const categories = require('./routers/categories');

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8880;

app.use('/users', users);
app.use('/products', products);
app.use('/categories', categories);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(port, () => {
        console.log('Server works on port: ', port);
    });

    exitHook(() => {
        console.log('Exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.log(e));