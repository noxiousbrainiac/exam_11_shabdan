const express = require('express');
const User = require("../models/User");

const router = express.Router();

router.post('/', async (req, res) => {
    try {
        const userData = {
            username: req.body.username,
            password: req.body.password,
            displayName: req.body.displayName,
            phoneNumber: req.body.phoneNumber
        }

        const user = new User(userData);
        user.generateToken();
        await user.save();
        res.send(user);
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post('/session', async (req, res) => {
    try {
        const user = await User.findOne({username: req.body.username});

        if (!user) return res.status(401).send({message: "Data is not correct"});

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) return res.status(401).send({message: "Data is not correct"});

        user.generateToken();
        await user.save({validateBeforeSave: false});
        res.send({message: "Username and password are correct!", user});
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/sessions' ,async (req, res) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();

        await user.save({validateBeforeSave: false});

        return res.send(success);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;