const Category = require('../models/Category');
const express = require('express');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const categories = await Category.find();

        if (!categories) {
            return res.status(404).send({error: "Nothing there"});
        }

        res.send(categories);
    } catch (e) {
        res.status(500).send(e);
    }
});

module.exports = router;