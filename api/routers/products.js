const express = require('express');
const Product = require('../models/Product');
const {nanoid} = require('nanoid');
const config = require('../config');
const multer = require('multer');
const path = require('path');
const auth = require("../middleware/auth");
const post = require("../middleware/post");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', [auth ,upload.single('image'), post], async (req, res) => {
    try {
        const product = new Product(req.data);
        await product.save();
        res.send(product);
    } catch (e) {
        res.status(500).send(e);
    }
});

router.get('/', async (req, res) => {
   try {
       if (req.query.category) {
           const products = await Product.find({category: req.query.category});
           if (!products) return res.status(404).send({error: "Data not found"});

           return res.send(products);
       }

       const products = await Product.find().populate('user', 'username');
       if (!products) return res.status(404).send({error: "Data not found"});
       res.send(products);
   } catch (e) {
       res.status(500).send(e);
   }
});

router.get('/:id', async (req, res) => {
    try {
        const product = await Product.findById(req.params.id).populate('user', 'username phoneNumber displayName');

        if (product) {
            res.send(product);
        } else if (!product){
            res.status(404).send({error: "Product not found"});
        }
    } catch (e) {
        res.status(500).send(e);
    }
});

router.delete('/:id', auth, async (req, res) => {
   try {
       const product = await Product.findById(req.params.id);

       if (req.user.id.toString() !== product.user.toString()) {
           return res.status(403).send({error: "You can't delete this product"});
       }

       if (product) {
           const product = await Product.findByIdAndDelete(req.params.id);
           res.send(`Product ${product.title} removed`);
       } else {
           res.status(404).send({error: 'Product not found'});
       }
   } catch (e) {
       res.status(500).send(e);
   }
});

module.exports = router;