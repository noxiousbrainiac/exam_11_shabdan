const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await  mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [admin, qwerty] = await User.create({
        username: "admin",
        password: "123",
        token: nanoid(),
        displayName: "Qwerty",
        phoneNumber: "996 776 78 78 45"
    }, {
        username: "qwerty",
        password: "123",
        token: nanoid(),
        displayName: "Asd",
        phoneNumber: "996 776 78 78 45"
    });

    const [computers, smartphones, cars, clothing] = await Category.create(
        {title: "Computers"},
        {title: "Smartphones"},
        {title: "Cars"},
        {title: "Clothing"},
    );

    await Product.create({
        title: "Lenovo",
        description: "China computer",
        image: "fixtures/lenovo.jpg",
        category: computers,
        price: 400,
        user: admin
    }, {
        title: "Iphone 13",
        description: "Apple's new product",
        image: "fixtures/iphone.jpg",
        category: smartphones,
        price: 799,
        user: qwerty
    }, {
        title: "Honda",
        description: "Honda's new product",
        image: "fixtures/honda.jpg",
        category: cars,
        price: 19799,
        user: qwerty
    }, {
        title: "Adidas Yeezy Boost 350",
        description: "Kanye West's new  design",
        image: "fixtures/adidas.jpeg",
        category: clothing,
        price: 299,
        user: admin
    });

    await mongoose.connection.close();
};

run().catch(e => console.log(e));